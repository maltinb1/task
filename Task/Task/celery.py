from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings



os.environ.setdefault('DJANGO_SETTINGS_MODULE', os.environ.get("DJANGO_SETTINGS_MODULE", "Task.settings.aws"))



app = Celery('Task')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

