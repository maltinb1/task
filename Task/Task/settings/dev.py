from .base import *
from celery.schedules import crontab

DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

# CELERY_BEAT_SCHEDULE = {
#     'job': {
#         'task': 'Task.tasks.update_kilometer',
#         'schedule': crontab()  # execute every minute
#     }
# }
#
#
#
# CELERY_BROKER_URL = 'redis://redis:6379'
# CELERY_RESULT_BACKEND = 'redis://redis:6379'
# CELERY_ACCEPT_CONTENT = ['application/json']
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'
#

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portfolio',
        'USER': 'databaseuser',
        'PASSWORD': 'databasepassword',
        'HOST':'db',
        'PORT': '3306',
    }
}


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'



