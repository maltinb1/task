from .base import *

#
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# SECURE_SSL_REDIRECT = True
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True


DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': os.environ.get("DATABASE_ENGINE", 'django.db.backends.mysql'),
#         'NAME': os.environ.get("DATABASE_NAME", None),
#         'USER': os.environ.get("DATABASE_USER", None),
#         'PASSWORD': os.environ.get("DATABASE_PASSWORD", None),
#         'HOST': os.environ.get("DATABASE_HOST", None),
#         'PORT': os.environ.get("DATABASE_PORT", None),
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES',character_set_connection=utf8,collation_connection=utf8_unicode_ci",
#         },
#     }
# }


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portfolio',
        'USER': 'databaseuser',
        'PASSWORD': 'databasepassword',
        'HOST':'db',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES',character_set_connection=utf8,collation_connection=utf8_unicode_ci",
        },
    }
}



EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
