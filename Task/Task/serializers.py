from rest_framework import serializers
from django.contrib.auth import get_user_model
from rest_framework import serializers
from registration import signals
from registration.models import RegistrationProfile
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
import django.contrib.auth.password_validation as validators


User = get_user_model()


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_new_password(self, value):
        validate_password(value)
        return value




class ProfileRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    # first_name = serializers.CharField(style={'input_type': 'text'}, write_only=True)
    # last_name = serializers.CharField(style={'input_type': 'text'}, write_only=True)


    class Meta:
        model = User
        fields = [
            # 'username',
            'email',
            'password',
            'password2',
            # 'first_name',
            # 'last_name',
        ]

    def validate(self,data):
        pw = data.get('password')
        pw2 = data.pop('password2')

        if pw != pw2:
            raise serializers.ValidationError("Passwords must match")
        return data

    def create(self, validated_data):

        user_obj = User(username=validated_data.get('email'),
                        email=validated_data.get('email'),
                         # first_name=validated_data.get('first_name'),
                         # last_name=validated_data.get('last_name'),
                        )
        user_obj.set_password(validated_data.get('password'))
        user_obj.save()



        # user = User.objects.create(user=user_obj)
        return user_obj

