from .serializers import ProfileRegisterSerializer, ChangePasswordSerializer
from rest_framework import generics, permissions
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model, logout as django_logout
from Task.backends import EmailAuthBackend

from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
from rest_framework.authentication import TokenAuthentication

from registration import signals
from registration.models import RegistrationProfile
from django.shortcuts import get_object_or_404
from django.contrib.sites.models import Site
from registration.backends.default.views import RegistrationView
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password


User = get_user_model()

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("email")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = EmailAuthBackend.authenticate(username,password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    # token, _ = Token.objects.get_or_create(user=user)
    # return Response({'token': token.key},
    #                 status=HTTP_200_OK)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)



class UpdatePassword(APIView):
    """
    An endpoint for changing password.
    """
    authentication_classes = [TokenAuthentication]
    serializer_class = ChangePasswordSerializer

    def get_object(self, queryset=None):
        my_token = self.request.META.get('HTTP_AUTHORIZATION').split()[1]
        print("My Token", my_token)
        user_id = Token.objects.get(key=my_token).user_id

        userx = get_object_or_404(User, id=user_id)
        print("user", userx)
        return userx
        # return self.request.user

    def put(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            old_password = serializer.data.get("old_password")
            if not self.object.check_password(old_password):
                return Response({"old_password": ["Wrong password."]},
                                status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response({'password':'changed'},status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileRegisterAPIView(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = ProfileRegisterSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs):
        site = "example.com"
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # We create a token than will be used for future auth
        token = Token.objects.create(user=serializer.instance)
        token_data = {"token": token.key}

        # user_username = self.request.data['username']
        # # print(user_username)
        # user = get_object_or_404(User, username=user_username)
        #
        # new_user = RegistrationProfile.objects.create_inactive_user(
        #     new_user=user,
        #     site=site,
        #     send_email=self.SEND_ACTIVATION_EMAIL,
        #     request=self.request,
        # )
        #
        # signals.user_registered.send(sender=self.__class__,
        #                              user=new_user, request=self.request)



        return Response(
            {**serializer.data, **token_data},
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class LogoutUserAPIView(APIView):
    authentication_classes = [TokenAuthentication]
    queryset = get_user_model().objects.all()

    def post(self, request):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)