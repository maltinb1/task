from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.http import HttpResponseRedirect
from django.views.defaults import page_not_found
from django.conf.urls import handler404 , handler500
from django.shortcuts import render, redirect, HttpResponse, Http404
from rest_framework import routers
from Task import views
from django.views.generic import TemplateView, RedirectView

from rest_framework_swagger.views import get_swagger_view


app_name = 'Task'


admin.site.site_title = "Task"

urlpatterns = [
    url(r'^management/administration/', admin.site.urls),
    url(r'^ProfileRegisterApi/$', views.ProfileRegisterAPIView.as_view(), name='auth_profile_create'),
    url(r'^login/$', views.login, name='login'),
    url(r'^auth/logout/$', views.LogoutUserAPIView.as_view(), name='auth_user_logout'),
    url(r'^auth/password_change/$', views.UpdatePassword.as_view(), name='auth_user_password_change'),
    # url('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^Drinkees/', include('Drinkees.urls')),
    url(r'^rest-auth/', include('rest_auth.urls')),







    url(r'^$', TemplateView.as_view(template_name="home.html"), name='home'),
    url(r'^signup/$', TemplateView.as_view(template_name="signup.html"),
        name='signup'),
    url(r'^email-verification/$',
        TemplateView.as_view(template_name="email_verification.html"),
        name='email-verification'),
    url(r'^login/$', TemplateView.as_view(template_name="login.html"),
        name='login'),
    url(r'^logout/$', TemplateView.as_view(template_name="logout.html"),
        name='logout'),
    url(r'^password-reset/$',
        TemplateView.as_view(template_name="password_reset.html"),
        name='password-reset'),
    url(r'^password-reset/confirm/$',
        TemplateView.as_view(template_name="password_reset_confirm.html"),
        name='password-reset-confirm'),

    url(r'^user-details/$',
        TemplateView.as_view(template_name="user_details.html"),
        name='user-details'),
    url(r'^password-change/$',
        TemplateView.as_view(template_name="password_change.html"),
        name='password-change'),


    # this url is used to generate email content
    url(r'^password-reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        TemplateView.as_view(template_name="password_reset_confirm.html"),
        name='password_reset_confirm'),

    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/', permanent=True), name='profile-redirect'),

]


# handler404 = 'Account.views.handler404'
# handler500 = 'Account.views.handler500'

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
