from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from TaskModels import views
# Create your views here.
app_name = 'TaskModels'

admin.site.site_title = "TaskModels"

urlpatterns = [
    url(r'^accounts/', include('registration.backends.default.urls')),
    # url(r'^CarApi/$', views.CarApiView.as_view(), name='car'),
    # url(r'^CarApi/(?P<pk>\d+)/$', views.CarDetailApiView.as_view(), name='carDetail'),
    # url(r'^CarDamageHistory/$', views.CarDamageHistoryApiView.as_view(), name='car_damage_history'),
    # url(r'^CarDamageHistory/(?P<pk>\d+)/$', views.CarDamageHistoryDetailApiView.as_view(), name='car_damage_history'),
    #
    # url(r'^EngineType/$', views.EngineTypeApiView.as_view(), name='engine'),
    # url(r'^EngineType/(?P<pk>\d+)/$', views.EngineTypeDetailApiView.as_view(), name='engineDetail'),
    # url(r'^Type/$', views.TypeApiView.as_view(), name='type'),
    # url(r'^Type/(?P<pk>\d+)/$', views.TypeDetailApiView.as_view(), name='typeDetail'),
    # url(r'^ManufacturerApi/$', views.ManufacturerApiView.as_view(), name='manufacturer'),
    # url(r'^Manufacturer/(?P<pk>\d+)/$', views.ManufacturerDetailApiView.as_view(), name='manufacturerDetail'),

]
              # + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
