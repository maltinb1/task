from django.contrib.auth.models import User
from rest_framework import generics, mixins , permissions
# from .serializers import CarsSerializer,EngineTypeSerializer,ManufacturerSerializer,TypeSerializer,CarsFilterSerializer,CarDamageHistorySerializer  # add this
# from TaskModels.models import Cars,EngineType,Manufacturer,Type,CarDamageHistory
from rest_framework.authentication import SessionAuthentication
from rest_framework.authentication import SessionAuthentication,TokenAuthentication
from rest_framework.filters import OrderingFilter, SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import status


# class CarDamageHistoryDetailApiView(generics.RetrieveUpdateDestroyAPIView):
#     # permissions_classes =[]
#     authentication_classes = [TokenAuthentication]
#
#     queryset = CarDamageHistory.objects.all()
#     serializer_class = CarDamageHistorySerializer
#
#
#
# class CarDamageHistoryApiView(mixins.CreateModelMixin,generics.ListAPIView):
#     serializer_class = CarDamageHistorySerializer
#     authentication_classes = [TokenAuthentication]
#     filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
#     filter_fields = ('car_id',)
#
#
#     def get_queryset(self):
#         qs = CarDamageHistory.objects.all()
#         return qs
#
#     def post(self,request,*args,**kwargs):
#         return self.create(request,*args,**kwargs)
#
# class CarApiView(mixins.CreateModelMixin,generics.ListAPIView):
#     serializer_class = CarsFilterSerializer
#     authentication_classes = [TokenAuthentication]
#
#     def get_queryset(self):
#         qs = Cars.objects.all()
#         return qs
#
#     def post(self,request,*args,**kwargs):
#         serializer = CarsSerializer(data=request.data)
#         if serializer.is_valid():
#             cars = serializer.save()
#             serializer = CarsSerializer(cars)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#     #
#     #     return self.create(request,*args,**kwargs)
#
#     # def perform_create(self,serializer):
#     #     serializer.save(user=self.request.user)
#
#
# class CarDetailApiView(generics.RetrieveUpdateDestroyAPIView):
#     # permissions_classes =[]
#     authentication_classes = [TokenAuthentication]
#
#     queryset = Cars.objects.all()
#     serializer_class = CarsSerializer
#
# #######
#
# class TypeApiView(mixins.CreateModelMixin,generics.ListAPIView):
#     serializer_class = TypeSerializer
#     authentication_classes = [TokenAuthentication]
#
#
#     def get_queryset(self):
#         qs = Type.objects.all()
#         return qs
#
#     def post(self,request,*args,**kwargs):
#         return self.create(request,*args,**kwargs)
#
#
# class TypeDetailApiView(generics.RetrieveUpdateDestroyAPIView):
#     # permissions_classes =[]
#     authentication_classes = [TokenAuthentication]
#     queryset = Type.objects.all()
#     serializer_class = TypeSerializer
#
# #######
#
#
#
# class EngineTypeApiView(mixins.CreateModelMixin,generics.ListAPIView):
#     serializer_class = EngineTypeSerializer
#     authentication_classes = [TokenAuthentication]
#     filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
#     filter_fields = ('manufacturer_id','type_id')
#
#
#     def get_queryset(self):
#         qs = EngineType.objects.all()
#         return qs
#
#     def post(self,request,*args,**kwargs):
#         return self.create(request,*args,**kwargs)
#
#
# class EngineTypeDetailApiView(generics.RetrieveUpdateDestroyAPIView):
#     # permissions_classes =[]
#     authentication_classes = [TokenAuthentication]
#     queryset = EngineType.objects.all()
#     serializer_class = EngineTypeSerializer
#
# #######
#
#
#
# class ManufacturerApiView(mixins.CreateModelMixin,generics.ListAPIView):
#     serializer_class = ManufacturerSerializer
#     authentication_classes = [TokenAuthentication]
#
#
#     def get_queryset(self):
#         qs = Manufacturer.objects.all()
#         return qs
#
#     def post(self,request,*args,**kwargs):
#         return self.create(request,*args,**kwargs)
#
#
# class ManufacturerDetailApiView(generics.RetrieveUpdateDestroyAPIView):
#     # permissions_classes =[]
#     authentication_classes = [TokenAuthentication]
#     queryset = Manufacturer.objects.all()
#     serializer_class = ManufacturerSerializer







