from django.apps import AppConfig


class TaskmodelsConfig(AppConfig):
    name = 'TaskModels'
