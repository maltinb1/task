from rest_framework import serializers
from .models import Cars,EngineType,Manufacturer,Type,CarDamageHistory


# class CarDamageHistorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = CarDamageHistory
#         fields = ('id',  'driverFault', 'manufacturerFault', 'autoParts', 'damageDetail','car','created_at')
#         # depth = 2
#
#
#
# class CarsSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Cars
#         fields = ('id','price', 'color', 'model','yearOfFabrication','currentKm','created_at','modified_at','is_deleted','engine')
#         # depth = 2
#
# class CarsFilterSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = Cars
#         fields = ('id','price', 'color', 'model','yearOfFabrication','currentKm','created_at','modified_at','is_deleted','engine')
#         depth = 2
#
# class EngineTypeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = EngineType
#         fields = ('id','manufacturer','type')
#         # depth = 1
#
#
# class TypeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Type
#         fields = ('id','name')
#
#
# class ManufacturerSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Manufacturer
#         fields = ('id','name')